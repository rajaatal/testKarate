Feature: Sample Test API
  Scenario: Test a Sample Get API
    Given url 'https://reqres.in/api/users?page=2'
    When method GET
    Then status 200
    And assert response.data[0].id==7

  Scenario:  Test a sample POST API
    Given url 'https://reqres.in/api/users'
    And request {id: 23,email: "ghizlanetalaoui@reqres.in",first_name: "Ghizlane",last_name: "Talaoui",avatar: "https://reqres.in/img/faces/6-image.jpg"}
    When method POST
    Then status 201
    And print response


